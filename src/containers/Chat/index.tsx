import React from "react";

import "./index.scss";

import Header from "../../components/Chat/Header";
import Content from "../../components/Chat/Content";
import Controls from "../../components/Chat/Controls";

import { ChatProvider } from "../../Context/ChatProvider";

function Chat() {
  return (
    <div className="Chat">
      <ChatProvider>
        <Header
          chatName="My awesome chat"
          participants={23}
          messages={53}
          lastMessageAt={"14:28"}
        />
        <Content />
        <Controls />
      </ChatProvider>
    </div>
  );
}

export default Chat;
