import React from "react";

import Logo from "../../components/Logo";
import Chat from "../Chat";

import "./index.scss";

function App() {
  return (
    <div className="App">
      <Logo />
      <Chat />
    </div>
  );
}

export default App;
