import React, { useReducer, useContext } from "react";
import { IMessage } from "../../interfaces/Message";

type Action =
  | {
      type: "SET_MESSAGES";
      data: Array<IMessage>;
    }
  | { type: "SET_CURRENT_MESSAGE"; data: string }
  | { type: "EDIT"; data: string }
  | { type: "EDIT_CANCEL" }
  | { type: "REMOVE_MESSAGE"; data: string }
  | { type: "UPDATE_MESSAGE" };
type Dispatch = (action: Action) => void;
type State = {
  messages: IMessage[];
  currentMessage: string;
  isEditing: boolean;
  editableMessage: IMessage;
};
type ProviderProps = { children: React.ReactNode };

const ChatStateContext = React.createContext<State | undefined>(undefined);
const ChatDispatchContext = React.createContext<Dispatch | undefined>(
  undefined
);

const reducer = (state: State, action: Action) => {
  switch (action.type) {
    case "SET_MESSAGES":
      return {
        ...state,
        messages: action.data,
      };
    case "SET_CURRENT_MESSAGE":
      return {
        ...state,
        currentMessage: action.data,
      };
    case "REMOVE_MESSAGE":
      const updMessages = [...state.messages].filter(
        (message) => message.id !== action.data
      );

      return {
        ...state,
        messages: [...updMessages],
      };
    case "EDIT":
      return {
        ...state,
        isEditing: true,
        editableMessage: {
          ...state.messages.find((message) => message.id === action.data),
        },
      };
    case "EDIT_CANCEL":
      return {
        ...state,
        isEditing: false,
        editableMessage: null,
        currentMessage: "",
      };
    case "UPDATE_MESSAGE":
      const messages = [...state.messages];
      messages.forEach((message) => {
        if (message.id === state.editableMessage.id) {
          message.text = state.currentMessage;
        }
      });
      return {
        ...state,
        messages: messages,
        currentMessage: "",
        isEditing: false,
        editableMessage: null,
      };
    default:
      throw new Error("Unknown 'user' action");
  }
};

const ChatProvider = ({ children }: ProviderProps) => {
  // @ts-ignore
  const [state, dispatch] = useReducer(reducer, {
    messages: [],
    currentMessage: "",
  });
  return (
    <ChatStateContext.Provider value={state}>
      <ChatDispatchContext.Provider value={dispatch}>
        {children}
      </ChatDispatchContext.Provider>
    </ChatStateContext.Provider>
  );
};

const useState = () => {
  const context = useContext(ChatStateContext);
  return context;
};

const useDispatch = () => {
  const context = useContext(ChatDispatchContext);
  return context;
};

export { ChatProvider, useState, useDispatch };
