import React from "react";

import "./index.scss";

function Spinner() {
  return <div className="ld-circle"></div>;
}

export default Spinner;
