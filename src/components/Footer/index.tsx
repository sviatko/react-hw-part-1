import React from "react";

import "./index.scss";

function Footer() {
  return <footer>&copy; Copyright 2020 | Sviatoslav Terletsky #BSA20</footer>;
}

export default Footer;
