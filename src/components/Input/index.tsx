import React from "react";

import "./index.scss";

import { useDispatch, useState } from "../../Context/ChatProvider";

function Input() {
  let value = "";
  const dispatch = useDispatch();
  const state = useState();

  if (state) {
    value = state.currentMessage;
  }

  return (
    <input
      type={"text"}
      name="message"
      className="input"
      value={value}
      onChange={(e) =>
        dispatch &&
        dispatch({
          type: "SET_CURRENT_MESSAGE",
          data: e.target.value,
        })
      }
    />
  );
}

export default Input;
