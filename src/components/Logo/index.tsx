import React from "react";

import logoImg from "../../logo.svg";
import "./index.scss";

function Logo() {
  return <img className="Logo" src={logoImg} alt="Logo" />;
}

export default Logo;
