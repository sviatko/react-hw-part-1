import React from "react";

import "./index.scss";

function Divider({ text }) {
  return (
    <div className="divider">
      <span className="divider-text">{text}</span>
    </div>
  );
}

export default Divider;
