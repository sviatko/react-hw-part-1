import React from "react";
import PropTypes from "prop-types";

import "./index.scss";

function Avatar({ src }: { src: string }) {
  return <img className="avatar" src={src} alt="Avatar" />;
}

export default Avatar;

Avatar.prototype = {
  src: PropTypes.string.isRequired,
};
