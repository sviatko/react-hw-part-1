import React from "react";

import Input from "../../Input";
import Button from "../../Button";

import { IMessage } from "../../../interfaces/Message";

import { me } from "../../../services/MessageService";
import { animateScroll } from "react-scroll";

import { useDispatch, useState } from "../../../Context/ChatProvider";

import "./index.scss";

function Controls() {
  const dispatch = useDispatch();
  const state = useState();

  let messages: IMessage[] | null = null;
  let message: string = "";

  if (state) {
    messages = state.messages;
    message = state.currentMessage;
  }

  const sendMessageHandler = () => {
    if (messages && message) {
      me(messages);
      // @ts-ignore
      const myMessage: IMessage = {
        ...me(messages),
        text: message,
        createdAt: new Date().toString(),
      };

      const mgsCopy = [...messages];
      mgsCopy.push(myMessage);

      if (dispatch) {
        console.log("here");
        dispatch({ type: "SET_MESSAGES", data: mgsCopy });
        dispatch({ type: "SET_CURRENT_MESSAGE", data: "" });
      }

      setTimeout(() => {
        animateScroll.scrollToBottom({
          containerId: "content",
        });
      }, 0);
    }
  };

  return (
    <div className="controls mt-3">
      <Input />
      <Button onClickHandler={sendMessageHandler} />
    </div>
  );
}

export default Controls;
