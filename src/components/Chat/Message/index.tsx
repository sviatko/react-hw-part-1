import React, { useState } from "react";
import PropTypes from "prop-types";
import moment from "moment";

import Avatar from "../Avatar";

import "./index.scss";

import { IMessage } from "../../../interfaces/Message";
import {
  useDispatch,
  useState as chatState,
} from "../../../Context/ChatProvider";

function Message(message: IMessage) {
  const isBen = message.user === "Ben";
  const className = ["message"];
  const [isLiked, setIsLiked] = useState(false);
  let onEditHandler = () => {};

  const dispatch = useDispatch();
  const state = chatState();

  let title = "Double click to toggle like";

  if (isBen) {
    className.push("right");
  } else {
    title = "Double click to remove the message";
    if (isLiked) {
      className.push("liked");
    } else {
      className.push("like");
    }
  }

  if (isBen) {
    if (
      state &&
      state.editableMessage &&
      state.editableMessage.id === message.id
    ) {
      className.push("editing");
    }

    if (dispatch) {
      onEditHandler = () => {
        dispatch({ type: "SET_CURRENT_MESSAGE", data: message.text });
        dispatch({ type: "EDIT", data: message.id });
      };
    }
  }

  const doubleClickHandler = () => {
    if (!isBen) {
      setIsLiked(!isLiked);
    } else {
      if (dispatch) {
        if (window.confirm("Are you sure you want to delete the message")) {
          dispatch({ type: "REMOVE_MESSAGE", data: message.id });
        }
      }
    }
  };
  return (
    <div
      className={className.join(" ")}
      title={title}
      onDoubleClick={doubleClickHandler}
      onClick={onEditHandler}
    >
      <Avatar src={message.avatar} />
      <span className="message-text">{message.text}</span>
      <span className="message-time">
        {moment(message.createdAt).format("H:m")}
      </span>
    </div>
  );
}

export default Message;

Message.prototype = {
  text: PropTypes.string,
  user: PropTypes.string,
  userId: PropTypes.string,
  avatar: PropTypes.string,
  createdAt: PropTypes.string,
  editedAt: PropTypes.string,
};
