import React from "react";
import PropTypes from "prop-types";

import moment from "moment";

import "./index.scss";

import { IHeader } from "../../../interfaces/Header";
import { useState } from "../../../Context/ChatProvider";

import { getTotaUsers } from "../../../services/MessageService";

function Header({ chatName }: IHeader) {
  let participants = 0;
  let messages = 0;
  let lastMessageAt = "00:00";

  const state = useState();

  if (state) {
    messages = state.messages.length;
    participants = getTotaUsers(state.messages);

    if (state.messages.length > 0) {
      lastMessageAt = moment(
        state.messages[state.messages.length - 1].createdAt
      ).format("H:m");
    }
  }
  return (
    <div className="header">
      <div>
        <span className="header-name mr-1">{chatName}</span>
        <span className="header-details mr-1">{participants} participants</span>
        <span className="header-details mr-1">{messages} messages</span>
      </div>
      <div>
        <span className="header-details">last message at {lastMessageAt}</span>
      </div>
    </div>
  );
}

export default Header;

Header.prototype = {
  chatName: PropTypes.string.isRequired,
};
