import React, { useEffect } from "react";

import "./index.scss";

import Message from "../Message";
import Divider from "../Divider";
import Spinner from "../../Spinner";

import { useDispatch, useState } from "../../../Context/ChatProvider";
import { getMessages } from "../../../services/ApiService";

import { groupByDay } from "../../../services/MessageService";

function Content() {
  const dispatch = useDispatch();
  const state = useState();

  let messages = null;

  useEffect(() => {
    setTimeout(() => {
      getMessages()
        .then((resp) => resp.json())
        .then((messages) =>
          dispatch ? dispatch({ type: "SET_MESSAGES", data: messages }) : ""
        )
        .catch((err) => console.log(err));
    }, 1000);
  }, []);

  if (state) {
    messages = state.messages;
  }

  let messagesGrouped = groupByDay(messages);
  let content: any = [];

  if (messages && messagesGrouped) {
    const keys = Object.keys(messagesGrouped);
    keys.map((day) => {
      content = [];
      if (messagesGrouped) {
        content.push(<Divider text={day} />);
        messagesGrouped[day].map((message) =>
          content.push(<Message key={message.createdAt} {...message} />)
        );
      }

      return;
    });

    if (keys.length === 0) {
      content.push(<Spinner />);
    }
  }

  return (
    <div id="content" className="content mt-3">
      {content && content.map((item: any) => item)}
    </div>
  );
}

export default Content;
