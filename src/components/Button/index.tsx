import React from "react";

import "./index.scss";

import { IButton } from "../../interfaces/Props/ButtonProps";
import { useDispatch, useState } from "../../Context/ChatProvider";

function Button({ onClickHandler }: IButton) {
  const state = useState();
  const dispatch = useDispatch();

  const updateHandler = () => {
    if (dispatch) dispatch({ type: "UPDATE_MESSAGE" });
  };

  let title = "";
  let onClick = null;
  let isEdt = false;
  let onEdit = () => {};

  if (state) {
    if (state.isEditing) {
      title = "Edit";
      onClick = updateHandler;
      isEdt = true;

      if (dispatch) {
        onEdit = () => {
          dispatch({ type: "EDIT_CANCEL" });
        };
      }
    } else {
      title = "Send";
      onClick = onClickHandler;
    }
  } else {
    onClick = onClickHandler;
  }

  return (
    <div className="button-group">
      <button className="button button-primary" onClick={onClick}>
        {title}
      </button>
      {isEdt && (
        <a href="#" className="button-cancel ml-1" onClick={onEdit}>
          Cancel
        </a>
      )}
    </div>
  );
}

export default Button;
