export interface IMessage {
  text: string;
  avatar: string;
  createdAt: string;
  editedAt: string;
  readonly id: string;
  user: string;
  userId: string;
}
