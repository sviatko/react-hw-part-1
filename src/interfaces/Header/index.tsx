export interface IHeader {
  chatName: string;
  participants: number;
  messages: number;
  lastMessageAt: string;
}
