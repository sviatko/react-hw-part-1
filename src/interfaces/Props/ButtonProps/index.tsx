import { IMessage } from "../../Message";

export interface IButton {
  onClickHandler: (event: any) => void;
}
