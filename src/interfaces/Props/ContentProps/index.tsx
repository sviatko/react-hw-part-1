import { IMessage } from "../../Message";

export interface IContentProps {
  messages: Array<IMessage> | null;
}
