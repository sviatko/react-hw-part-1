import { IMessage } from "../../Message";

export interface IControlsProps {
  messages: Array<IMessage> | null;
  setMessages: (messages: Array<IMessage>) => void;
}
