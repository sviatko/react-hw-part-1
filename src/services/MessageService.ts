import { IMessage } from "../interfaces/Message";
import { IGroupedMessages } from "../interfaces/GroupedMessages";
import moment from "moment";

export const groupByDay = (
  messages: IMessage[] | null
): IGroupedMessages<IMessage> | null => {
  if (!messages) {
    return null;
  }

  const FORMAT = "ddd, MMM Do";

  const groups = messages.reduce(
    (groups: IGroupedMessages<IMessage>, message: IMessage) => {
      let createdAt = moment(message.createdAt).format(FORMAT);

      const now = moment();
      const createdDay = moment(message.createdAt);

      switch (now.diff(createdDay, "days")) {
        case 1:
          createdAt = "Yesterday";
          break;
        case 0:
          createdAt = "Today";
          break;
      }

      if (!groups[createdAt]) {
        groups[createdAt] = [];
      }
      groups[createdAt].push(message);

      return groups;
    },
    {}
  );

  return groups;
};

export const me = (messages: IMessage[]) =>
  messages.find((message) => message.user === "Ben");

export const getTotaUsers = (messages: IMessage[]) => {
  const tmpArrCounter: string[] = [];
  messages.forEach((message) => {
    if (!tmpArrCounter.includes(message.userId)) {
      tmpArrCounter.push(message.userId);
    }
  });

  return tmpArrCounter.length;
};
